package sg.ncs.product.demo_12may.common.exception;

public class RemoteApiException extends RuntimeException {

    public RemoteApiException(String message) {
        super(message);
    }
}
